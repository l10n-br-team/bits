#### RASCUNHO #####

# BITS da equipe debian-l10n-portuguese - Mes/Ano

Mensalmente emitimos um relatório da 
[equipe debian-l10n-portuguese](https://wiki.debian.org/Brasil/Traduzir) do
Projeto [Debian](https://www.debian.org) e este é o xxxxxxx deles. O objetivo é
que as atividades da equipe sejam conhecidas e possamos dar os devidos créditos
pelo trabalho produzido por nossos contribuidores. Acreditamos que isso poderá
estimular a participação de novos tradutores e revisores.


## Visão geral de <Mês> de <Ano>

 * *Verificar* Atualização da wiki sobre como traduzir as páginas web.
 * *Verificar* Traduções DDTP.
 * XX Páginas traduzidas/atualizadas no site oficial.
 * XX Traduções PO.
 * XX Traduções de páginas wiki
 * XX Traduções de posts da equipe de publicidade.

## Pessoas com atividades registradas durante setembro de 2018

 *
 *
 *
 *
 *

Se você fez alguma colaboração de tradução que não está listada acima, nos avise
na lista (debian-l10n-portuguese@lists.debian.org) para que seu trabalho seja
devidamente registrado e reconhecido!

## Itens traduzidos

 *
 *
 *
 *
 *

A equipe de tradução para português do Brasil agradece a todos que 
colaboram para que o Debian se torne mais universal!

Venha [ajudar](https://wiki.debian.org/Brasil/Traduzir) você também. 
Toda ajuda é bem-vinda e estamos precisando da sua.

Não perca a [DebConf19 no Brasil](https://debconf19.debconf.org/). A DebConf 
acontecerá em Curitiba entre os dias 21 e 28 de Julho de 2019. Participe!
